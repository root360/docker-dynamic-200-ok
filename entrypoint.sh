#!/bin/bash

set -e

echo ">> DOCKER-ENTRYPOINT: GENERATING SSL CERT"
openssl genrsa -des3 -passout pass:password -out /tmp/server.pass.key 2048
openssl rsa -passin pass:password -in /tmp/server.pass.key -out /etc/ssl/private/server.key
openssl req -new -key /etc/ssl/private/server.key -out /tmp/server.csr -subj "/C=DE/ST=Leipzig/L=Leipzig/O=root360.cloud/OU=root360.cloud/CN=nginx.root360.cloud"
openssl x509 -req -sha256 -days 300065 -in /tmp/server.csr -signkey /etc/ssl/private/server.key -out /etc/ssl/certs/server.crt
rm /tmp/server.pass.key /tmp/server.csr
echo ">> DOCKER-ENTRYPOINT: GENERATING SSL CERT ... DONE"

echo ">> DOCKER-ENTRYPOINT: GENERATING NGINX CONFIG"
cat > /etc/nginx/conf.d/default.conf << EOF
server {
  listen ${HTTP_LISTEN_PORT:-80};
  server_name nginx.root360.cloud;
  location / {
    default_type text/plain;
    return 200 "${CONTENT:-Hello world}\n";
  }
  rewrite ^/.* /;
}
server {
  listen ${HTTPS_LISTEN_PORT:-443} ssl http2;
  ssl_certificate /etc/ssl/certs/server.crt;
  ssl_certificate_key /etc/ssl/private/server.key;
  server_name nginx.root360.cloud;
  location / {
    default_type text/plain;
    return 200 "${CONTENT:-Hello world}\n";
  }
  rewrite ^/.* /;
}
EOF
echo ">> DOCKER-ENTRYPOINT: GENERATING NGINX CONFIG ... DONE"

echo ">> DOCKER-ENTRYPOINT: STARTING CATCHALL NGINX ON HTTP/${HTTP_LISTEN_PORT:-80} AND HTTPS/${HTTPS_LISTEN_PORT:-443}"
exec nginx -g 'daemon off;'
