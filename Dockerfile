FROM nginx:alpine

RUN apk --no-cache add bash openssl

COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
