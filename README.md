# docker-dynamic-200-ok

The running container has a catchall rewrite which responds with 200 to all requests.

By default it runs on port 80/443 but can be configured using container environment variable `HTTP_LISTEN_PORT`/`HTTPS_LISTEN_PORT`.

You can modify the result by adding the variable `CONTENT` (default: `Hello world`).